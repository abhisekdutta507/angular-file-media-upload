import { TestBed } from '@angular/core/testing';

import { MediaCoverterService } from './media-coverter.service';

describe('MediaCoverterService', () => {
  let service: MediaCoverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MediaCoverterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

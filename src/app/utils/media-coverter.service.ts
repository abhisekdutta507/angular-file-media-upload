import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MediaCoverterService {

  constructor() { }

  public async urltoFile(url: string, filename: string, mimeType: string): Promise<File> {
    return fetch(url)
        .then(r => { return r.arrayBuffer(); })
        .then(buf => { return new File([buf], filename, { type: mimeType }); });
  }
}

import { Component, OnInit } from '@angular/core';
import { APIResponse } from 'src/app/interfaces/apiresponse';
import { MediaCoverterService } from 'src/app/utils/media-coverter.service';
import { FaceImage } from 'src/assets/face_image';
import { HttpService } from 'src/app/services/http.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-media-upload',
  templateUrl: './media-upload.component.html',
  styleUrls: ['./media-upload.component.scss']
})
export class MediaUploadComponent implements OnInit {
  private baseURL: string = 'http://localhost:5000';
  public imageURL: string = '';
  private imageName: string;

  constructor(private mediaConverter: MediaCoverterService, private http: HttpService) {
    this.imageName = 'face_image.png';
  }

  ngOnInit(): void {
    this.imageURL = FaceImage.imageAsBase64;
  }

  /**
   * @returns appends all the input file items into a form object
   */
  getFormDataWithInputs(formdata: FormData, inputs: NodeListOf<HTMLInputElement>): FormData {
    inputs.forEach((item: HTMLInputElement) => {
      let i = 0;
      while(item.files?.length && i < item.files.length) {
        formdata.append('media', item.files[i++]);
      }
    });
    return formdata;
  }

  /**
   * @description submit image files with fetch
   */
  public async handleClickWithFileThroughFetch(e: MouseEvent) {
    if (!e.isTrusted) {
      return;
    }

    let formdata: FormData = new FormData();
    const inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll('input[name="media"]');
    formdata = this.getFormDataWithInputs(formdata, inputs);

    const requestOptions: RequestInit = {
      method: 'POST',
      body: formdata
    };
    
    let r: APIResponse = {};
    try {
      r = await fetch(`${this.baseURL}/api/v1/uploadmedia`, requestOptions).then(async r => await r.json());
    } catch(e) {
      // error detected
    }
  }

  /**
   * @description submit base64 image files with fetch
   */
  public async uploadBase64ImageThroughFetch(e: MouseEvent) {
    if (!e.isTrusted) {
      return;
    }

    const formdata: FormData = new FormData();
    const file: File = await this.mediaConverter.urltoFile(this.imageURL, this.imageName, FaceImage.mimeType);
    formdata.append('media', file);

    const requestOptions: RequestInit = {
      method: 'POST',
      body: formdata
    };
    
    let r: APIResponse = {};
    try {
      r = await fetch(`${this.baseURL}/api/v1/uploadmedia`, requestOptions).then(async r => await r.json());
    } catch(e) {
      // error detected
    }
  }

  /**
   * @description submit image files with Angular HttpClient
   */
  public async handleClickWithFileThroughHttpClient(e: MouseEvent) {
    if (!e.isTrusted) {
      return;
    }

    let headers: HttpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'enctype': 'multipart/form-data'
    });
    let formdata: FormData = new FormData();
    const inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll('input[name="media"]');
    formdata = this.getFormDataWithInputs(formdata, inputs);

    const http$ = this.http.uploadmedia(formdata, headers);
    http$.subscribe((r: APIResponse) => {
      // success
    });
  }

  /**
   * @description submit base64 image files with Angular HttpClient
   */
  public async uploadBase64ImageThroughHttpClient(e: MouseEvent) {
    if (!e.isTrusted) {
      return;
    }

    let headers: HttpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'enctype': 'multipart/form-data'
    });
    const formdata: FormData = new FormData();
    const file: File = await this.mediaConverter.urltoFile(this.imageURL, this.imageName, FaceImage.mimeType);
    formdata.append('media', file);

    const http$ = this.http.uploadmedia(formdata, headers);
    http$.subscribe((r: APIResponse) => {
      // success
    });
  }
}

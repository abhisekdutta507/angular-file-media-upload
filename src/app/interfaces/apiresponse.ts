export interface APIResponse {
    uploaded?: boolean,
    fileUrls?: string[]
}

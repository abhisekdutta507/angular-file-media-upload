import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MediaUploadComponent } from './components/media-upload/media-upload.component';

const routes: Routes = [
  {
    path: 'media-upload',
    component: MediaUploadComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'media-upload'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseURL: string = 'http://localhost:5000';

  constructor(private http: HttpClient) { }

  uploadmedia(body: any, headers: HttpHeaders): Observable<any> {
    return this.http.post<any>(`${this.baseURL}/api/v1/uploadmedia`, body, { headers });
  }
}
